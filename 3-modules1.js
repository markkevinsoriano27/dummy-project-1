//modules

const names = require ('./4-names')
const sayHi = require ('./5-utils')
const alternative = require ('./alternative')

require('./7-mind-grenade')

// sayHi('john')
// sayHi(names.Mark)
// sayHi(names.Anne)